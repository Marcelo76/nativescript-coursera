import { Component, OnInit, Input } from '@angular/core';
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { PageRoute } from 'nativescript-angular/router';
import { HotelesService } from '../domain/hoteles.service';

@Component({
  selector: 'ns-detalle',
  templateUrl: './detalle.component.html'
})
export class DetalleComponent implements OnInit {
    //@Input() id: number;
    private id: number = 7;
    public comentarios: {idHotel: number, comentario: string}[];

  constructor(
      private pageRoute: PageRoute,
      private _hotelesService: HotelesService
      ) {
    //console.log(this.pageRoute.activatedRoute.getValue['id']);
  }

  ngOnInit(): void {
      this.comentarios = this._hotelesService.getReviews(this.id)
  }

  onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}