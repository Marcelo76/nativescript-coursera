import { Injectable } from '@angular/core';
import { getJSON, request } from  'tns-core-modules/http';
const Sqlite = require('nativescript-sqlite');
//import Sqlite from "nativescript-sqlite";

@Injectable({
  providedIn: 'root'
})
export class NoticiasService {
    private api: string = "https://39c1dcd9c2ef.ngrok.io";  //IMPORTANTE: Correr ngrok http 3000 desde la carpeta de ngrok y actualizar ésta url
    private database: any;

  constructor() {
      //this.getDb();
   }



   getDb(){
    (new Sqlite("my.db")).then(db => {
        db.execSQL("CREATE TABLE IF NOT EXISTS logs (ID INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)")
            .then(id => {
            this.database = db;
        }, error => {
            console.log("CREATE TABLE ERROR", error);
        }).then(
            this.getData()
        );
    }, error => {
        console.log("OPEN DB ERROR", error);
    });
   }

   private getData(){
    this.database.all("SELECT * FROM people").then(rows => {
        //this.people = [];
        for(var row in rows) {
            console.log({"firstname": rows[row][1], "lastname": rows[row][2]})
            //this.people.push({
            //    "firstname": rows[row][1],
            //    "lastname": rows[row][2]
            //});
        }
    }, error => {
        console.log("SELECT ERROR", error);
    });
   }


    /*
  getDb(fnOk, fnError){
    return new sqlite("mi_db_logs",(err, db) => {
        if(err){
            console.log("Error al abrir la Base de datos.", err);
        }else{
            console.log('Estado de la base de datos:', db.isOpen() ? "Abierta" : "Cerrada");
            db.execSql("CREATRE TABLE IF NOT EXISTS logs (ID INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)")
                .then((id) => {
                    console.log("TABLA CREADA");
                    fnOk(db)
                },(error) => {
                    console.log("Error al intentar crear la tabla:", error);
                    fnError(error);
                })
        }
    });
  }
  */

  agregar(noticia: string){
      return request({
          url: this.api + '/fav',
          method: "POST",
          headers: {"Content-Type": "application/json"},
          content: JSON.stringify({
            nuevo: noticia
          })
      });
  }

  favs(){
      return getJSON(this.api + '/get?q=');
  }

  buscar(texto: string = ''){
    /*
        this.database.execSQL(
            "INSERT INTO logs (texto) VALUES (?)",
            [texto])
            .then(
                (err, id) => console.log("nuevo Id", id)
        );
    */
   console.log(this.api + '/get?q=' + texto);
    return getJSON(this.api + '/get?q=' + texto);
  }
  /*
  private noticias: string[] = [];

  constructor() { }

  agregar(noticia: string): boolean{
    this.noticias.push(noticia);
    return true;
  }

  buscar(){
    return this.noticias;
  }

  eliminar(noticia: string): boolean{
    this.noticias = this.noticias.map(n => {if(n !== noticia){return n}}).filter(n => n !== undefined);
    return true;
  }
  */
}
