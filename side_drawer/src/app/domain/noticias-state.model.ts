import { Injectable, Inject } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';


//ESTADO
export class Noticia{
    constructor(public titulo: string) {}
}

export interface NoticiasState{
    items: Noticia[],
    noticia: Noticia,
    sugerida: Noticia,
    lecturas: String[]
}

export function initializeNoticiasState(){
    return {
        items: [],
        noticia: null,
        sugerida: null,
        lecturas: []
    }
}

//ACCIONES
export enum NoticiasActionTypes{
    INIT_MY_DATA = "[Noticias] Init My Data",
    NUEVA_NOTICIA = "[Noticias] Nueva",
    SUGERIR_NOTICIA = "[Noticias] Sugerir",
    LISTAR_LECTURAS = "[Noticias] Listar Lecturas"
}

export class InitMyDataAction implements Action{
    type = NoticiasActionTypes.INIT_MY_DATA;
    constructor(public titulares: string[]){};
}

export class NuevaNoticiaAction implements Action{
    type = NoticiasActionTypes.NUEVA_NOTICIA;
    constructor(public noticia: Noticia){}
}


export class SugerirActionimplements implements Action {
    type = NoticiasActionTypes.SUGERIR_NOTICIA;
    constructor(public noticia: Noticia){}
}

export class ListarLecturasActionimplements implements Action {
    type = NoticiasActionTypes.LISTAR_LECTURAS;
    constructor(public lectura: String){}
}

export type NoticiasViajesActions = InitMyDataAction | NuevaNoticiaAction | SugerirActionimplements | ListarLecturasActionimplements;

export function reducerNoticias(
    state: NoticiasState,
    action: NoticiasViajesActions
): NoticiasState{
    switch(action.type){
        case NoticiasActionTypes.INIT_MY_DATA:{
            const titulares: Array<String> = (action as InitMyDataAction).titulares;
            return {
                ...state,
                items: titulares.map((t) => new Noticia(<string>t))
            };
        }
        case NoticiasActionTypes.NUEVA_NOTICIA:{
            return {
                ...state,
                items: [...state.items, (action as NuevaNoticiaAction).noticia]
            };
        }
        case NoticiasActionTypes.SUGERIR_NOTICIA:{
            return {
                ...state,
                sugerida: (action as SugerirActionimplements).noticia
            };
        }
        case NoticiasActionTypes.LISTAR_LECTURAS:{
            return {
                ...state,
                lecturas: [...state.lecturas,(action as ListarLecturasActionimplements).lectura]
            };
        }
    }

    return state;
}

@Injectable()
export class NoticiasEffects{
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(NoticiasActionTypes.NUEVA_NOTICIA),
        map((action: NuevaNoticiaAction) => new SugerirActionimplements(action.noticia))
    )

    constructor(private actions$: Actions){}
};
