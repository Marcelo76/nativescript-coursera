import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HotelesService {
    private hoteles: {id: number, nombre: string, categoria: string}[] = [
            {"id": 1, "nombre":"Hotel casa del rio","categoria":"Hotel"},
            {"id": 2, "nombre":"Hotel vista Hermosa","categoria":"Hotel"},
            {"id": 3, "nombre":"Hotel Plaza","categoria":"Hotel"},
            {"id": 4, "nombre":"Hotel Casino","categoria":"Hotel"},
            {"id": 5, "nombre":"Hotel Alameda","categoria":"Hotel"},
            {"id": 6, "nombre":"Hotel Marbella","categoria":"Hotel"},
            {"id": 7, "nombre":"Hotel Amalfi","categoria":"Hotel"}
        ];

    private comentarios: {idHotel: number, comentario: string}[] = [
        {"idHotel": 1, "comentario": "Buenisima atención y acogedor"},
        {"idHotel": 1, "comentario": "Buenisima atención pereo un poco desordenado"},
        {"idHotel": 1, "comentario": "Buenisima atención, pero falta aseo"},
        {"idHotel": 2, "comentario": "Me encantó"},
        {"idHotel": 2, "comentario": "La comida es buenísima y variada"},
        {"idHotel": 2, "comentario": "El bar tiene poca variedad en tragos"},
        {"idHotel": 2, "comentario": "El casino es lo mejor"},
        {"idHotel": 2, "comentario": "Demasiado caro"},
        {"idHotel": 3, "comentario": "Bueno, bonito y barato"},
        {"idHotel": 3, "comentario": "Recomendable"},
        {"idHotel": 3, "comentario": "La disco es lo mejor, muy buen ambiente"},
        {"idHotel": 3, "comentario": "Gran variedad de actividades"},
        {"idHotel": 3, "comentario": "Las habitacionbes son preciosas."},
        {"idHotel": 4, "comentario": "Gran hotel, buena atención a buen precio"},
        {"idHotel": 4, "comentario": "Muy acogedor, volvería nuevamente"},
        {"idHotel": 4, "comentario": "El casino es lo mejor del hotel, gran variedad de actividades."},
        {"idHotel": 5, "comentario": "Un poco lejos de los atractivos de la ciudad."},
        {"idHotel": 5, "comentario": "Me encantó."},
        {"idHotel": 5, "comentario": "La barra es lo mejor, junto con el restorant"},
        {"idHotel": 6, "comentario": "Cercano a la playa, y atraciones turísticas"},
        {"idHotel": 6, "comentario": "Buena comida, lo mejor"},
        {"idHotel": 6, "comentario": "La playa queda en frente, hermosa vista"},
        {"idHotel": 6, "comentario": "Buen precio, y con gran cantidad de actividades"},
        {"idHotel": 7, "comentario": "Caro pero bueno"},
        {"idHotel": 7, "comentario": "Bonitas habitaciones, nada mal"},
        {"idHotel": 7, "comentario": "Genial, pero carito"},
        {"idHotel": 7, "comentario": "Buena barra, buen restorant, super bueno"}
    ];

  constructor() { }

  getHotels(){
      return this.hoteles;
  }

  getReviews(idHotel: number):{idHotel: number, comentario: string}[]{
      return this.comentarios.filter(c => c.idHotel === idHotel);
  }

  agregar(nombreHotel: string, categoria: string = "Hotel"){
    let id: number = Math.floor(Math.random() * 1000);
    this.hoteles.push({id, nombre: nombreHotel + id.toString(), categoria});
  }

  actualizarCategoria(id: number, categoria: string): boolean{
    this.hoteles.forEach(h => {if(h.id == id){h.categoria = categoria}} );
    return true;
  }

  eliminar(id: number):boolean{
      this.hoteles.forEach(h => h.id !== id);
      return true;
  }
}
