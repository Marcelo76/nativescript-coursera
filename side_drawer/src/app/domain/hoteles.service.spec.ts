import { TestBed } from '@angular/core/testing';

import { HotelesService } from './hoteles.service';

describe('HotelesService', () => {
    //El siguiente código es válido para angular 8
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HotelesService = TestBed.get(HotelesService);
    expect(service).toBeTruthy();
  });
});
