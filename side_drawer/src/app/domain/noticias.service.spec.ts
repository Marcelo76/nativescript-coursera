import { TestBed } from '@angular/core/testing';

import { NoticiasService } from './noticias.service';
//Para angular 8 Instalar npm i @types/jest 

describe('NoticiasService', () => {
  //El siguiente código es válido para angular 8 
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NoticiasService = TestBed.get(NoticiasService);
    expect(service).toBeTruthy();
  });


  /* El siguiente código es generado por ng pero es válido para angular 9 o superior
  let service: NoticiasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NoticiasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  */
});
