import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", loadChildren: () => import("~/app/home/home.module").then((m) => m.HomeModule) },
    { path: "browse", loadChildren: () => import("~/app/browse/browse.module").then((m) => m.BrowseModule) },
    { path: "search", loadChildren: () => import("~/app/search/search.module").then((m) => m.SearchModule) },
    { path: "featured", loadChildren: () => import("~/app/featured/featured.module").then((m) => m.FeaturedModule) },
    { path: "settings", loadChildren: () => import("~/app/settings/settings.module").then((m) => m.SettingsModule) },
    { path: "page1", loadChildren: () => import("~/app/page1/page1.module").then((m) => m.Page1Module) },
    { path: "page2", loadChildren: () => import("~/app/page2/page2.module").then((m) => m.Page2Module) },
    { path: 'listado', loadChildren : () => import("~/app/listado/listado.module").then((m) => m.ListadoModule)},
    { path: 'detalle', loadChildren: () => import("~/app/detalle/detalle.module").then((m)=> m.DetalleModule)},
    { path: 'crear-usuario',loadChildren: () => import("~/app/usuarios/form-usuarios/usuarios.module").then(m => m.UsuariosModule)}
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
