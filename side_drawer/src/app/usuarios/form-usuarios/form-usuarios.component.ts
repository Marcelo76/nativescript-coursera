import { Component, OnInit } from '@angular/core';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import * as app from "tns-core-modules/application";
import { RouterExtensions } from 'nativescript-angular/router';
import { NoticiasService } from '~/app/domain/noticias.service';
import * as Toast from 'nativescript-toast';
import * as appSettings from "tns-core-modules/application-settings";

@Component({
  selector: 'ns-form-usuarios',
  templateUrl: './form-usuarios.component.html'
})
export class FormUsuariosComponent implements OnInit {
    public userName: string;

  constructor(
      private routerExtensions: RouterExtensions,
      private _noticiasService: NoticiasService
    ) { }

  ngOnInit(): void {
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }


  grabar(userName: string){
    appSettings.setString("nombreUsuario", userName);
    console.log('Nombre usuario',this.userName);
    this._noticiasService.agregar(this.userName).then(r=>{
        this.showToast('El registro ha sido ingresado.');
        this.backToSetting();
    },e=>{
        console.log(e);
        this.showToast('Erro al intentar ingresar el usuario: ' + e.message);
    });
  }

  backToSetting(){
    this.routerExtensions.navigate(['/settings'],{
        transition: {name: "fade"}
    })
  }

  private showToast(mensaje: string){
    let toast = Toast.makeText(mensaje);
    toast.show();
  }
}
