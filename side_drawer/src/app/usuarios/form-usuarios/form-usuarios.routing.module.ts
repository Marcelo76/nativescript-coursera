import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { FormUsuariosComponent } from "./form-usuarios.component";

const routes: Routes = [
    { path: "", component: FormUsuariosComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class UsuariosRoutingModule {}
