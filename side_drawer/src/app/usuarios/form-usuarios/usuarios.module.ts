import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { FormUsuariosComponent } from './form-usuarios.component';
import { UsuariosRoutingModule } from './form-usuarios.routing.module';



@NgModule({
  declarations: [
    FormUsuariosComponent
  ],
  imports: [
    NativeScriptCommonModule,
    UsuariosRoutingModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class UsuariosModule { }
