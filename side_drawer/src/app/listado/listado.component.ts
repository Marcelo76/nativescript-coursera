import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import * as app from "tns-core-modules/application";
import { RouterExtensions } from 'nativescript-angular/router';
import { HotelesService } from '../domain/hoteles.service';
import * as Toast from 'nativescript-toast';
import * as dialogs from 'tns-core-modules/ui/dialogs';

@Component({
  selector: 'ns-listado',
  templateUrl: './listado.component.html'
})
export class ListadoComponent implements OnInit {

  constructor(
      public _hotelesService: HotelesService,
      private routerExtensions: RouterExtensions) { }

  ngOnInit(): void {
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }

  verDetalle(e): void{
    this.routerExtensions.navigate(['/detalle', {id: e.index}],{
        transition: {
            name: "fade"
        }
    })
  }

  onPull(e){
      setTimeout(() => {
        this._hotelesService.agregar("Nuevo Hotel ");
        e.refreshing = false;
        this.showToast('Registro creado...');
      },2000);
  }

  addCategory(id: number){
      dialogs.action('Selecciona la categoría','Cancelar',['Hotel','Motel','Cabaña','Casa particular'])
      .then((result) => {
          if(result !== "Cancelar"){
            this._hotelesService.actualizarCategoria(id, result);
          }
      });
  }


  eliminar(id: number){
    console.log(id);
    dialogs.action("¿Desea eliminar el hotel?","Cancelar",["Eliminar"])
    .then((result)=>{
        console.log(result);
        if(result === "Eliminar"){
            if(this._hotelesService.eliminar(id)){
                dialogs.alert({
                    title: "Eliminar",
                    message: "El hotel ha sido eliminado",
                    okButtonText: "Aceptar"
                });
            }
        }
    });
  }



  private showToast(mensaje: string){
    let toast = Toast.makeText(mensaje);
    toast.show();
  }
}
