import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { device } from "tns-core-modules/platform";
import { AppState } from "../app.module";
import { Store } from "@ngrx/store";
import { map } from 'rxjs/operators';

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {
    public plataforma: string = "No pude detectar tu dispositivo.";
    public msg: string;
    public lecturas: string[] = [];


    constructor(private store: Store<AppState>) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.store.select(state => state.noticias.lecturas).subscribe((lectura: string[]) => {
            this.lecturas = lectura;
            console.log("Lecturas", this.lecturas)
        });
        // Init your component properties
        console.log(device.os);
        if(device.os === 'Android'){
            this.plataforma = "Estás en un dispositivo Android";
            this.msg = "Bienvenido a Android";
        }
        if(device.os === 'IOS'){
            this.plataforma = "Estás en un dispositivo IOS";
        }
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
