import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
//import { SearchFormComponent } from "./search/search-form.component";
//import { ListadoComponent } from './listado/listado.component';
//import { DetalleComponent } from './detalle/detalle.component';
//import { Page1Component } from './page1/page1.component';
//import { Page2Component } from './page2/page2.component';
import { EffectsModule } from '@ngrx/effects';
import { NoticiasState, reducerNoticias, initializeNoticiasState, NoticiasEffects } from './domain/noticias-state.model';
import { ActionReducerMap, StoreModule as NgRxStoreModule } from "@ngrx/store";


//Init Redux
export interface AppState{
    noticias: NoticiasState
}

const reducers: ActionReducerMap<AppState> = {
    noticias: reducerNoticias
}

const reducerInitialState = {
    noticias: initializeNoticiasState()
}
//End Redux


@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        NativeScriptModule,
        NativeScriptUISideDrawerModule,
        NgRxStoreModule.forRoot(reducers, { initialState: reducerInitialState}),
        EffectsModule.forRoot([NoticiasEffects])
    ],
    declarations: [
        AppComponent,
        //SearchFormComponent
        //ListadoComponent,
        //DetalleComponent,
        //Page1Component,
        //Page2Component
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
