import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as dialogs from 'tns-core-modules/ui/dialogs';
import * as Toast from 'nativescript-toast';
import { RouterExtensions } from "nativescript-angular/router";
import * as appSettings from "tns-core-modules/application-settings";

@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {
    public nombreUsuario: string;

    constructor(private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    doLater(fn){setTimeout( fn, 1000)}

    ngOnInit(): void {
        // Init your component properties here.
        this.nombreUsuario = appSettings.getString("nombreUsuario", "Anónimo");

        this.doLater(() => {
            dialogs.action("Mensaje", "Cancelar", ["Opción 1", "Opción 2"])
            .then((result) => {
                console.log(result);
                if(result == "Opción 1"){
                    this.doLater(() =>
                        dialogs.alert({
                            title: 'Titulo Opción 1',
                            message: 'Mensaje Opción 1',
                            okButtonText: 'Aceptar opción 1'
                        }).then(() => {
                            console.log("Cerrando 1!"),
                            this.showToast("Mensaje desde la opción 1.")
                        })
                    );

                }else if(result == "Opción 2"){
                    this.doLater(()=>
                        dialogs.alert({
                            title: 'Titulo Opción 2',
                            message: 'Mensaje opción 2',
                            okButtonText: 'Aceptar Opción 2'
                        }).then(() => {
                            console.log("Cerrando 2!"),
                            this.showToast("Mensaje desde la opción 2.")
                        })
                    );
                }
            });
        });


    }


    private showToast(mensaje: string){
        //Implementación de toast extraida desde https://www.npmjs.com/package/nativescript-toast
        var toast = Toast.makeText(mensaje);
        //toast.setDuration(Toast.duration.long); //Para un mensaje con una duración más larga
        this.doLater(()=>toast.show());

        /*
        //Esta forma de implementar el toast está obsoleta
        const toastOptions = Toast.ToastOptions = {text: "Hello Word", duration: Toast.DURATION_SHORT};
        this.doLater(() => toastOptions.show(toastOptions));
        */
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    goToNewUser(){
        this.routerExtensions.navigate(['/crear-usuario'],{
            transition: {name: "fade"}
        })
    }



}
