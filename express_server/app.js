var express = require('express');
var cors = require('cors');
var app = express();
app.use(express.json());
app.use(cors());
app.listen(3000, () => console.log("Servidor corriendo en el puerto 3000"));

var noticias = [
    "Literatura Paris", "Futbol Barcelona", "Futbol Barranquilla", 
    "Política Montevideo", "Economía Santiago de Chile", "Cocina Mexico DF",
    "Finanzas Nueva York"
];

app.get("/get", (req, rest, next) => 
    rest.json(noticias.filter((c)=> c.toLowerCase().indexOf(req.query.q.toString().toLowerCase())>-1))
);

var misFavoritos = [];

app.get("/fav", (req, rest, next) => rest.json(misFavoritos));
app.post("/fav", (req, res, next) => {
    console.log(req.body);
    misFavoritos.push(req.body.nuevo);
    res.json(misFavoritos);
});